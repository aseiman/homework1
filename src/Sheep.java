import java.util.Arrays;
import java.util.Random;

public class Sheep {

   enum Animal {sheep, goat};
   
   public static void main (String[] param) {

	   // for debugging

   }
   
   public static void reorder (Animal[] animals) {
	   /**
	    * Sorteerib sisendmassiivi nii, et sikud jäävad massiivi ette otsa ja
	    * lambad massiivi lõppu.
	    * @param animals Sorteerimata massiiv lammastest ja sikkudest
	    * @return Sorteeritud massiiv, kus sikud asuvad massiivi ees otsas ja lambad lõpus 
	    */
	   
	   // Lõpetab, kui massiiv liiga lühike
	   if (animals.length < 2) {
		   return;
	   }	   
	   
	   // Initseerib lammaste ja sikkude otsimise algpunktid
	   int i = 0;
	   int j = animals.length - 1;	   	  
	   	   
	   while (true) {

		   // Otsib vasakult, kus asub mõni lammas
		   while (animals[i] == Sheep.Animal.goat) {
			   i++;			  
			   if (i == animals.length - 1) { break; } // Lõpetab, kui jõuab massiivi lõppu			   
		   }
		   
		   // Otsib paremalt, kus asub mõni sikk
		   while (animals[j] == Sheep.Animal.sheep) {
			   j--;
			   if (j == 0) { break; } // Lõpetab, kui jõuab massiivi algusesse
		   }
		   
		   // Lõpetab, kui sikkude ja lammaste otsimine kohtuvad
		   if (i >= j) { break; } 
		   
		   // Vahetab leitud lamba ja siku asukohad
		   Sheep.Animal tmp = animals[i];
		   animals[i] = animals[j];
		   animals[j] = tmp;
		   
		   // Liigutakse ühe võrra edasi
		   i++;
		   j--;
	   } 	   
   }
}

